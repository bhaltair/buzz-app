import './App.css';
import React from 'react';
import { Button, Divider, Input, Form, Col, Row } from 'antd';
import ReactJson from 'react-json-view'
// const { Header, Footer, Sider, Content } = Layout;
import { KJUR, hextob64, KEYUTIL } from 'jsrsasign'

const PEM_BEGIN = '-----BEGIN PRIVATE KEY-----\n'
const PEM_END = '\n-----END PRIVATE KEY-----'

let SignUtil = {
  /**
   * rsa签名
   * @param content 签名内容
   * @param privateKey 私钥，PKCS#1
   * @param hash hash算法，SHA256withRSA，SHA1withRSA
   * @returns 返回签名字符串，base64
   */
  rsaSign: function (content, privateKey, hash) {
    privateKey = this._formatKey(privateKey)
    const key = KEYUTIL.getKey(privateKey);
    // console.log(key)

    // 创建 Signature 对象
    const signature = new KJUR.crypto.Signature({
      alg: hash,
    })
    signature.init(key)

    signature.updateString(content)
    const signData = signature.sign()
    // 将内容转成base64
    const result = hextob64(signData)

    return result
  },
  _formatKey: function (key) {
    if (!key.startsWith(PEM_BEGIN)) {
      key = PEM_BEGIN + key
    }
    if (!key.endsWith(PEM_END)) {
      key = key + PEM_END
    }
    return key
  }
}

async function postData(url = '', data = null, headers = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      ...headers
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: data ? JSON.stringify(data) : null // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

// function generateString(length) {
//   let result = '';
//   const characters = '0123456789';
//   const charactersLength = characters.length;
//   for (let i = 0; i < length; i++) {
//     result += characters.charAt(Math.floor(Math.random() * charactersLength));
//   }
//   return result;
// }

function App() {
  const [form] = Form.useForm();
  const [jsonObj, setJsonObj] = React.useState({})

  const onFinish = (values) => {
    const body = values["Request-Body"]
    const privateKey = values["Private-Key"]
    const clientId = values["Client-Id"]
    const keyVersion = values["Private-Key-Version"]
    const Url = values["Url"]
    const time = Date.now()
    const pathName = new URL(Url)?.pathname

    const contentToBeSigned = body ? `POST ${pathName}\n${clientId}.${time}.${body}` : `POST ${pathName}\n${clientId}.${time}.`

    const sign = encodeURIComponent(SignUtil.rsaSign(contentToBeSigned, privateKey, "SHA256withRSA"))


    postData(Url, body ? JSON.parse(body) : null, {
      'Content-Type': 'application/json',
      'Client-Id': clientId,
      'Request-Time': time,
      'Signature': `algorithm=RSA256,keyVersion=${keyVersion},signature=${sign}`
    })
      .then(data => {
        console.log(data); // JSON data parsed by `data.json()` call
        setJsonObj(data)
      });
  };

  return (
    <div className="App" style={{
    }}>
      <Row>
        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
          <Form
            layout={"vertical"}
            form={form}
            initialValues={{
              layout: "vertical",
              "Url": "https://api.pay.dev.vibra.work/v1/api/webhook/certificates",
              "Client-Id": "LIVE_3ee88b5690124cedbb3f2050313aabd5",
              "Private-Key-Version": "2",
              "Private-Key": "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDWvPV10uj9vqBYgx4NG25Qkncg6/kyMyW7WNSJsri9mAg3dpU5ln3ecw8h7iEeDuOjSHhq3QZdWEQ5hhqFxAwte0sjWMGNkSeOn5iTM4w3gG8JVvmIY1ZsR8S4kbg5Ih+iPoQ84hv7Y3WnrL59yc58YrAuEX5wLIibairQ2feStW01452r4yI5lOMwP8ECBIo/GQ3mcy8GoOhubbduh8emy4vm8saMXLzj1Ks9MXPXO0DSenb8SVS+pukcAGYtDWgAuLm3Mb0E63ZvZOo4AwHpwjMaT6hRB14VOojdqAJIpIlh7SoSeIgVMgSruSkdx4GOwcPrjeG7JO9Ux3vy5YO3AgMBAAECggEAaO+qG8vXY7h17wsT8l/HRsRsRsBJ5IguFiFQq4GU2cGjskbWoW+zTrN3X6NYt7Ga+D3/6mjcJV1u6ZVo/JlTQh6QF1PWMfIPHJlNHBICoVS37h58jnPM1dk0fuVm2zSbtNNMF9x+xBhUdH3sENg/eXVYlwqhePuqPhhhl/IME7qDlD7rCm17Ad8mLCK5ts7lXrLDvYaGJ1UK839Juu3cHzRljvHCApqqvVJ+PCoOmlZcH1P1T9jq8V8M8GE8TT5L0CHBnhAkxCt/YJB3Hb8T5R058uFDnC0/69cuuUdxnl/OGGmuU/tPoXPDij8di7wNrTJXoBLvXLVZwuz9UJ+yIQKBgQD2jyDx+7z8oyXBXtFvNuISxWbtyMCmTxxnJEMPrRzqQSLQ5N8beyOSSoTQPy02HA5UxDhUaH4nUtJCdfjWnnm8KhoLijEwpYqcMw1HGO4nFr07LefQ8FyTCwhxzLCCHXJVBm3pqknQnPAy+m6kzi0ocnIUQNUYFTewEYd+bUhfGQKBgQDe9eh8UuSCOLeNi1YyQT3mJAU02U2H2s6uuHFvm3/hHRLtFa6vx9JPrD8nDZ4YEAbejJZJG4mbvsmNsoes23g2jeHkSxNbwOm4jPM5r3VnOH75Cqziow3pbCOmOeyXIKY3789z1wHgT3qlHuSNqBQHqasTzmJl/Owf0GStt8XjTwKBgFefuEqnd5ulc5aMVwwaSi/3ncPl86lnNiXlujuI/1y1N2Vb4XqHwbWM7DSCL1QqlmFDtu0wkZzyDy11dbD4czUPFWCChUbPnjoy3oYk8wZLS4cLonYb5hqP8jlXUGd9hZN4DHndfgw4qhjD7ax+ZYgZXDecbt73M2pMYIXq2qyJAoGASDjl5ye+x4PxyVLiSAMkNuFiIl1lpOU24Bf0/s8jHFubntrQLDaRJKfKCS97D5ZZc0uc/u+BosapaVH0awToqttI2feLvMP/sX1O6Feysw30610O5Zcl0X/rCFRpFAdKcIIo9AAopm14b/ZS2KQiYN0QAqlu7FSCdEe/5RnLazcCgYBqDzh0QpW+zJnXrmeWTE4/1zsE3BR5b7nht2xBknCRSn/TFD1PLERSwxBvkphugB3XXBtTMUJQARnvacsDZQ39ZSjZJUuG/+55wg/Xc1h8q8mSMdkH7tP6hyfJ2/v+4J7KN9j3pnjigPY12Y8lnxvw9N59AWAQ1tUh7yeWdcj4+A==",
              // "Request-Body": ``
            }}
            onFinish={onFinish}
          >
            <Divider orientation="left">Request Header</Divider>
            <Form.Item label="Url" name={"Url"} rules={[{ required: true, message: 'Please input url!' }]}>
              <Input placeholder="" />
            </Form.Item>
            <Form.Item label="Client-Id" name={"Client-Id"} rules={[{ required: true, message: 'Please input client id!' }]}>
              <Input placeholder="" />
            </Form.Item>
            <Form.Item label="Private-Key" name={"Private-Key"} rules={[{ required: true, message: 'Please input private key!' }]}>
              <Input.TextArea rows={2} placeholder="" />
            </Form.Item>
            <Form.Item label="Private-Key-Version" name={"Private-Key-Version"} rules={[{ required: true, message: 'Please input key version!' }]}>
              <Input placeholder="" />
            </Form.Item>
            <Divider orientation="left">Request Body</Divider>
            <Form.Item label="Request Body" name={"Request-Body"}>
              <Input.TextArea rows={4} placeholder="" />
            </Form.Item>
            <Form.Item >
              <Button type="primary" htmlType="submit">Send</Button>
            </Form.Item>
          </Form>
        </Col>
        <Col xs={24} sm={24} md={24} lg={12} xl={12}>
          <Divider orientation="left">Response</Divider>
          <ReactJson src={jsonObj} name={null} indentWidth={2} />
        </Col>
      </Row>
    </div>
  );
}

export default App;
